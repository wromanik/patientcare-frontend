import {Component, OnInit} from '@angular/core';
import {Patient} from "./patient";
import {HttpErrorResponse} from "@angular/common/http";
import {PatientService} from "./patient.service";
import {NgForm} from "@angular/forms";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit{

  title = 'patientcare';

  // @ts-ignore
  public patients: Patient[];
  // @ts-ignore
  public editPatient: Patient;
  // @ts-ignore
  public deletePatient;

  refresh(): void {
    window.location.reload();
  }

  constructor(private patientService: PatientService) {  }

  ngOnInit() {
    this.getPatients();
  }

  public getPatients():void{
    this.patientService.getPatients().subscribe(
      (response: Patient[]) => {
        this.patients = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onAddPatient(addForm: NgForm): void {
    // @ts-ignore
    document.getElementById('add-patient-form').click();

    this.patientService.addPatient(addForm.value).subscribe(
      (response: Patient) => {
        console.log(response);
        this.getPatients();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }


  public onUpdatePatient(patient: Patient): void {
    this.patientService.updatePatient(patient).subscribe(
      (response: Patient) => {
        console.log(response);
        this.getPatients();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onDeletePatient(patientId: number): void {
    this.patientService.deletePatient(patientId).subscribe(
      (response: void) => {
        console.log(response);
        this.getPatients();
      },
      (error: HttpErrorResponse) => {
          alert(error.message);
      }
    );

  }

  public searchPatients(key: string): void {
    console.log(key);
    const results: Patient[] = [];
    for (const patient of this.patients) {
      if (patient.name.toLowerCase().indexOf(key.toLowerCase()) !== -1
        || patient.surname.toLowerCase().indexOf(key.toLowerCase()) !== -1
        || patient.email.toLowerCase().indexOf(key.toLowerCase()) !== -1
        || patient.phone.toLowerCase().indexOf(key.toLowerCase()) !== -1) {
        results.push(patient);
      }
    }
    this.patients = results;


    if (results.length === 0 || !key) {
      setTimeout(function(){}, 5000);

      this.refresh();
    }
  }

  public onOpenModal(patient: Patient, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (mode === 'edit') {
      this.editPatient = patient;
      button.setAttribute('data-target', '#updatePatientModal');
    }
    if (mode === 'delete') {
      this.deletePatient = patient;
      button.setAttribute('data-target', '#deletePatientModal');
    }
    //@ts-ignore
    container.appendChild(button);
    button.click();
  }





}




