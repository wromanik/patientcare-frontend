import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {PatientService} from "./patient.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import { Component, OnInit } from '@angular/core';

import {FormsModule} from "@angular/forms";
declare var $: any;




@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [PatientService],
  bootstrap: [AppComponent]
})
export class AppModule {


}
