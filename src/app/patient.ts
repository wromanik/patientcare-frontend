export interface Patient{
  id: number;
  name: string;
  surname: string;
  email: string;
  phone: string;
  imgUrl: string;
  patientCode: string;
}
